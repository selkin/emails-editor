# emails-editor
EmailsEditor is a simple text component for work with emails.
It has the following advantages:
* It doesn't have dependencies
* It has a very simple API

![preview](docs/preview.gif)

## Example
You can see an example [here](https://selkin.gitlab.io/emails-editor/example.html)

## Getting started
```
    <script charset="utf-8" src="/public/output/emails-editor.min.js"></script>
    <link media="all" rel="stylesheet" href="/public/output/emails-editor.min.css" />
    <div id="emails-editor-container"></div>
    <script>
        const emailsEditor = EmailsEditor({
            container: document.querySelector('#emails-editor-container')
        });
        emailsEditor
            .on('CHANGE_EMAIL_LIST', function() {
                console.log('actual emails after changes:', this.getEmails());
            })
            .on('DELETE_EMAIL', function(email) {
                console.log('email "' + email + '" was deleted!');
            })
            .init();

        emailsEditor.addEmails(['example@domain.com', 'example2@domain2.com']);

        setTimeout(() => {
            emailsEditor.destroy();
        }, 5000);
    </script>
```
Also you can use the component in your project through any bundler, like that:

```
import EmailsEditor from './email-editor.min';
import from './email-editor.min.css';

const emailsEditor = EmailsEditor({
    container: document.querySelector('#emails-editor-container')
});
```

## API

# Options for constructor
When you create the component, you have to pass the following settings:
* `container: HTMLElement`: DOM element, that will be used as container for the component;

# Supported public methods

Once instance of `EmailsEditor` is created, the following methods are available for you:

* `init(): this`: The method initializes the component and renders it to DOM;
* `destroy(): this`: The method removes the component from DOM;
* `addEmails(emails: string[]): this`: The method adds emails to the component;
* `addEmail(email: string): this`: The method adds only one email to the component;
* `setEmails(emails: string[]): this`: The method changes emails to the component;
* `removeEmail(email: string): this`: The method removes email to the component;
* `getEmails(): string[]`: The method returns all emails from the component
* `getValidEmails(): string[]`: The method returns only valid emails from the component
* `on(event: string, handler: Function): this`: It adds a handler to a specific event.
Please, see below for examples.
* `off(event: string, handler: Function): this`: It removes event handler.

For example:
```
const inst = new EmailsEditor({ container: document.querySelector("#app") });
inst.init().addEmails(['test@test.com','test2@test.com']);
```

## Supported events

* `CHANGE_EMAIL_LIST`: This event is fired after changes email list;
* `DELETE_EMAIL`: This event is fired after the email is deleted. This event won't be triggered, when you use `setEmails` method!;

```
const inst = new EmailsEditor({ container: document.querySelector("#app") });
inst
    .on('CHANGE_EMAIL_LIST', () => {
        console.log(`actual email list: ${inst.getEmails()}!`);
    })
    .on('DELETE_EMAIL', email => {
        console.log(`${email} was deleted!`);
    })
    .init()
    .addEmails(['test@test.com','test2@test.com']);
```

## Getting started

```
npm i && npm start
```

Open in browser `http://localhost:3000/public/localhost.html`
