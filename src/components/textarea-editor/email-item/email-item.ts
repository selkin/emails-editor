import { emailIsValid, isShallowEqual } from '../../../utils/common';
import { DELETE_EMAIL, CHANGE_EMAIL } from '../../../consts/events';
import { BaseUIComponent } from '../../base-ui-component';

import css from './email-item.css';


export interface EmailItemOpts {
    container: HTMLElement;
    email: string;
}

const DATA_ATTR_NAME = 'data-email-item';

const DATA_VALUE_CONTENT = 'content';
const DATA_VALUE_INPUT = 'input';
const DATA_VALUE_DELETE = 'delete';


export class EmailItem extends BaseUIComponent {

    private email = '';
    private prevEmail = this.email;
    private container: HTMLElement;
    private wrapperElem: HTMLElement | null = null;


    private onClickDelegationHandler = (e: MouseEvent) => {
        const deleteButtonElem = this.deleteButtonElem;
        if (!e.target || !deleteButtonElem) {
            return;
        }

        const isDeleting = deleteButtonElem.contains(e.target as HTMLElement);

        if (isDeleting) {
            this.emit(DELETE_EMAIL);
        }
    }

    private onKeyDownHandler = (e: KeyboardEvent) => {
        const inputElem = e.target as HTMLInputElement;

        if (!inputElem) {
            return;
        }

        if (e.code === 'Enter') {
            e.preventDefault();
            this.setBlurState();
            inputElem.blur();
        }
    }

    private onInputHandler = (e: Event) => {
        const inputElem = e.target as HTMLInputElement;
        const contentElem = this.contentElem;

        if (!inputElem || !contentElem) {
            return;
        }

        contentElem.textContent = this.email = inputElem.value;
    }

    private onFocusHandler = (_: Event) => {
        this.setFocusState();
    }

    private onBlurHandler = (_: Event) => {
        if (!isShallowEqual([this.prevEmail], [this.email])) {
            this.emit(CHANGE_EMAIL, this.prevEmail);
            this.prevEmail = this.email;
        }
        this.setBlurState();
    }


    constructor({ email, container }: EmailItemOpts) {
        super();
        this.email = this.prevEmail = email;
        this.container = container;
    }

    init(): this {
        const wrapperElem = document.createElement('div');

        wrapperElem.className = css.wrapper;
        wrapperElem.innerHTML = this.getTemplate();
        this.wrapperElem = wrapperElem;

        wrapperElem.addEventListener('click', this.onClickDelegationHandler);

        const inputElem = this.inputElem;

        if (inputElem) {
            inputElem.value = this.email;

            inputElem.addEventListener('input', this.onInputHandler);
            inputElem.addEventListener('focus', this.onFocusHandler);
            inputElem.addEventListener('blur', this.onBlurHandler);
            inputElem.addEventListener('keydown', this.onKeyDownHandler);
        }

        this.container.before(wrapperElem);

        if (this.contentElem) {
            this.contentElem.textContent = this.email;
        }

        this.updateValidationState();

        return this;
    }

    getEmail(): string {
        return this.email;
    }

    destroy(): this {
        const inputElem = this.inputElem;

        if (inputElem) {
            inputElem.removeEventListener('input', this.onInputHandler);
            inputElem.removeEventListener('focus', this.onFocusHandler);
            inputElem.removeEventListener('blur', this.onBlurHandler);
            inputElem.removeEventListener('keydown', this.onKeyDownHandler);
        }

        const wrapperElem = this.wrapperElem;
        if (wrapperElem) {
            wrapperElem.removeEventListener('click', this.onClickDelegationHandler);
            wrapperElem.remove();
        }

        this.clear();

        return this;
    }

    private get contentElem(): HTMLParagraphElement | null {
        return this.wrapperElem && this.wrapperElem.querySelector(`[${DATA_ATTR_NAME}="${DATA_VALUE_CONTENT}"]`) || null;
    }

    private get inputElem(): HTMLInputElement | null {
        return this.wrapperElem && this.wrapperElem.querySelector(`[${DATA_ATTR_NAME}="${DATA_VALUE_INPUT}"]`) || null;
    }

    private get deleteButtonElem(): HTMLSpanElement | null {
        return this.wrapperElem && this.wrapperElem.querySelector(`[${DATA_ATTR_NAME}="${DATA_VALUE_DELETE}"]`) || null;
    }

    private setFocusState(): void {
        const wrapperElem = this.wrapperElem;
        if (!wrapperElem) {
            return;
        }
        wrapperElem.classList.add(css.focusedWrapper);
    }

    private setBlurState(): void {
        const wrapperElem = this.wrapperElem;

        if (!wrapperElem) {
            return;
        }
        wrapperElem.classList.remove(css.focusedWrapper);
        this.updateValidationState();
    }

    private getTemplate(): string {
        return `
            <p class="${css.content}" ${DATA_ATTR_NAME}="${DATA_VALUE_CONTENT}"></p>
            <input type="text" class="${css.input}" ${DATA_ATTR_NAME}="${DATA_VALUE_INPUT}" />
            <button class="${css.delete}" ${DATA_ATTR_NAME}="${DATA_VALUE_DELETE}"></button>
        `;
    }

    private updateValidationState(): void {
        const wrapperElem = this.wrapperElem;

        if (!wrapperElem) {
            return;
        }

        if (emailIsValid(this.email)) {
            wrapperElem.classList.remove(css.errorWrapper);
        } else {
            wrapperElem.classList.add(css.errorWrapper);
        }
    }
}