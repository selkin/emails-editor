import { splitByComma, emailIsValid, isShallowEqual } from '../../utils/common';
import { CHANGE_EMAIL_LIST, DELETE_EMAIL, CHANGE_EMAIL } from '../../consts/events';
import { BaseUIComponent } from '../base-ui-component';
import { EmailItem } from './email-item/email-item';

import css from './textarea-editor.css';


export interface TextAreaEditorOpts {
    container: HTMLElement;
}

const DATA_ATTR_NAME = 'data-textarea-editor';

const DATA_VALUE_INPUT = 'input';
const DATA_VALUE_INPUT_CONTENT = 'input-content';
const DATA_VALUE_INPUT_WRAPPER = 'input-wrapper';


export class TextAreaEditor extends BaseUIComponent {

    private containerElem: HTMLElement;
    private emails: EmailItem[] = [];


    private onClickDelegationHandler = (e: MouseEvent) => {
        const inputElem = this.inputElem;
        if (e.target && e.target === this.containerElem && inputElem) {
            inputElem.focus();
        }
    }

    private onInputHandler = (e: Event) => {
        const contentElem = this.contentElem;

        if (!e.target || !contentElem) {
            return;
        }

        contentElem.textContent = (e.target as HTMLInputElement).value;
    }

    private onKeyDownHandler = (e: KeyboardEvent) => {
        const inputElem = e.target as HTMLInputElement;
        const isEnter = e.code === 'Enter';
        const isComma = e.key === ',';

        if (!inputElem || !inputElem.value || !isEnter && !isComma) {
            return;
        }

        this.addEmail(inputElem.value);

        if (isComma) {
            e.preventDefault();
        }
    }

    private onPasteHandler = (e: ClipboardEvent) => {
        const value = e.clipboardData && e.clipboardData.getData('text/plain') || '';
        const emails = splitByComma(value);

        this.addEmails(emails);
        e.preventDefault();
    }

    private onBlurHandler = (e: Event) => {
        const inputElem = e.target as HTMLInputElement;

        if (!inputElem) {
            return;
        }

        if (inputElem.value) {
            this.addEmail(inputElem.value);
        }
    }


    constructor(opts: TextAreaEditorOpts) {
        super();
        this.containerElem = opts.container;
    }

    init(): this {
        this.containerElem.innerHTML = this.getTemplate();
        this.containerElem.classList.add(css.container);

        this.containerElem.addEventListener('click', this.onClickDelegationHandler);

        const inputElem = this.inputElem;

        if (inputElem) {
            inputElem.addEventListener('input', this.onInputHandler);
            inputElem.addEventListener('blur', this.onBlurHandler);
            inputElem.addEventListener('keydown', this.onKeyDownHandler);
            inputElem.addEventListener('paste', this.onPasteHandler);
        }

        return this;
    }

    destroy(): this {
        const inputElem = this.inputElem;

        this.containerElem.removeEventListener('click', this.onClickDelegationHandler);

        if (inputElem) {
            inputElem.removeEventListener('input', this.onInputHandler);
            inputElem.removeEventListener('blur', this.onBlurHandler);
            inputElem.removeEventListener('keydown', this.onKeyDownHandler);
            inputElem.removeEventListener('paste', this.onPasteHandler);
        }

        this.emails.forEach(e => e.destroy());
        this.emails = [];

        this.clear();

        this.containerElem.innerHTML = '';

        return this;
    }

    setEmails(emails: string[]): this {
        const before = this.getEmails();
        this.emails.forEach(e => {
            e.destroy();
        });
        this.emails = [];
        this.addEmails(emails, false);
        this.compareAndEmitChangeEvent(before);
        return this;
    }

    addEmails(emails: string[], shouldEmitChange: boolean = true): this {
        const before = this.getEmails();
        emails.forEach(email => this.addEmail(email, false));

        if (shouldEmitChange) {
            this.compareAndEmitChangeEvent(before);
        }
        return this;
    }

    removeEmail(email: string): this {
        const foundEmails = this.emails
            .filter(e => e.getEmail() === email);

        foundEmails.forEach(e => {
            e.destroy();
            this.emails = this.emails.filter(e => e.getEmail() !== email);
            this.emit(DELETE_EMAIL, email);
            this.emitChangeEvent();
        });
        return this;
    }

    addEmail(email: string, shouldEmitChange: boolean = true): this {
        const inputWrapperElem = this.inputWrapperElem;
        const contentElem   = this.contentElem;
        const inputElem     = this.inputElem;

        if (!inputWrapperElem || !contentElem || !inputElem) {
            return this;
        }

        const newEmail = new EmailItem({
            container: inputWrapperElem,
            email
        });
        this.emails.push(newEmail);

        newEmail
            .on(DELETE_EMAIL, () => {
                this.emit(DELETE_EMAIL, newEmail.getEmail());
                newEmail.destroy();
                this.emails = this.emails.filter(e => e !== newEmail);
                this.emitChangeEvent();
            })
            .on(CHANGE_EMAIL, () => {
                this.emitChangeEvent();
            })
            .init();

        if (shouldEmitChange) {
            this.emitChangeEvent();
        }

        inputElem.value = '';
        contentElem.textContent = '';

        return this;
    }

    getEmails(): string[] {
        return this.emails.map(e => e.getEmail());
    }

    getValidEmails(): string[] {
        return this.getEmails()
            .reduce((res, email) => {
                if (emailIsValid(email)) {
                    res.push(email);
                }
                return res;
            }, [] as string[]);
    }

    private get inputElem(): HTMLInputElement | null {
        return this.containerElem.querySelector(`[${DATA_ATTR_NAME}="${DATA_VALUE_INPUT}"]`) || null;
    }

    private get contentElem(): HTMLParagraphElement | null {
        return this.containerElem.querySelector(`[${DATA_ATTR_NAME}="${DATA_VALUE_INPUT_CONTENT}"]`) || null;
    }

    private get inputWrapperElem(): HTMLParagraphElement | null {
        return this.containerElem.querySelector(`[${DATA_ATTR_NAME}="${DATA_VALUE_INPUT_WRAPPER}"]`) || null;
    }

    private compareAndEmitChangeEvent(before: string[]): void {
        const now = this.emails.map(e => e.getEmail());
        if (!isShallowEqual(before, now)) {
            this.emitChangeEvent();
        }
    }

    private emitChangeEvent(): void {
        this.emit(CHANGE_EMAIL_LIST);
    }

    private getTemplate(): string {
        return `
            <div
                class="${css.inputWrapper}"
                ${DATA_ATTR_NAME}="${DATA_VALUE_INPUT_WRAPPER}"
            >
                <p class="${css.inputContent}" ${DATA_ATTR_NAME}="${DATA_VALUE_INPUT_CONTENT}"></p>
                <input
                    class="${css.input}"
                    type="text"
                    placeholder="add more people..."
                    spellcheck="false"
                    ${DATA_ATTR_NAME}="${DATA_VALUE_INPUT}"
                />
            </div>
        `;
    }

}
