import { EventEmitter } from '../utils/event-emitter';


export abstract class BaseUIComponent extends EventEmitter {
    abstract init(): this;
    abstract destroy(): this;
}