import { splitByComma, emailIsValid, isShallowEqual } from './common';


describe('common utilities (utils/common.ts) should work correctly', () => {

    describe('"splitByComma" should work correctly', () => {
        test('should return list of emails', () => {
            expect(splitByComma('ivan@mail.ru​, max@mail.ru').join(''))
                .toEqual(['ivan@mail.ru', '​max@mail.ru'].join(''));
        });

        test('should return list with all emails', () => {
            expect(splitByComma('ivan+mail.ru, max@mail.ru').join(''))
                .toEqual(['ivan+mail.ru', 'max@mail.ru'].join(''));
        });

        test('should return empty list for invalid string', () => {
            expect(splitByComma(',,,,,,,').join(''))
                .toEqual([].join(''));
        });
    });

    describe('"emailIsValid" should work correctly', () => {
        test('should return right value', () => {
            expect(emailIsValid('adadad@domain.com'))
                .toStrictEqual(true);
        });

        test('should return right value for subdomain', () => {
            expect(emailIsValid('adadad@sub.domain.com'))
                .toStrictEqual(true);
        });

        test('should return right value for invalid email', () => {
            expect(emailIsValid('adadad+sub.domain.com'))
                .toStrictEqual(false);
        });
    });

    describe('"isShallowEqual" should work correctly', () => {
        test('should return right value for strings', () => {
            expect(isShallowEqual(['a', 'b'], ['a', 'b']))
                .toStrictEqual(true);
            expect(isShallowEqual([], []))
                .toStrictEqual(true);
            expect(isShallowEqual([1, 2], [1, 2]))
                .toStrictEqual(true);
        });

        test('should depend on order elements', () => {
            expect(isShallowEqual(['a', 'b'], ['b', 'a']))
                .toStrictEqual(false);
            expect(isShallowEqual([1, 2], [2, 1]))
                .toStrictEqual(false);
        });

        test('should return right value for different arrays', () => {
            expect(isShallowEqual(['a', 'b'], ['a']))
                .toStrictEqual(false);
            expect(isShallowEqual([1], [1, 2]))
                .toStrictEqual(false);
        });
    });

});
