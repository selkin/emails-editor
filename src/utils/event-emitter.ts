type EventEmitterListeners = { [event: string]: Function[] };


export class EventEmitter {

    protected listeners: EventEmitterListeners = {};


    on(event: string, handler: Function): this {
        this.listeners[event] = this.listeners[event] || [];
        this.listeners[event].push(handler);
        return this;
    }

    off(event: string, handler: Function): this {
        this.listeners[event] = this.listeners[event] || [];
        this.listeners[event] = this.listeners[event].filter(h => h !== handler);
        return this;
    }

    clear(): this {
        this.listeners = {};
        return this;
    }

    protected emit(event: string, ...args: any[]) {
        (this.listeners[event] || [])
            .forEach(handler => handler.apply(this, args));
    }

}