import { TextAreaEditor, TextAreaEditorOpts } from './components/textarea-editor/textarea-editor';


const factory = (opts: TextAreaEditorOpts) => new TextAreaEditor(opts);

export default factory;