const path                 = require('path');
const postcssNested        = require('postcss-nested');
const postcssImport        = require('postcss-import');
const webpack              = require('webpack');
const autoprefixer         = require('autoprefixer');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSPlugin    = require('optimize-css-assets-webpack-plugin');
const TerserPlugin         = require('terser-webpack-plugin');

const isProd  = process.env.NODE_ENV === 'production';
const isWatch = process.env.WATCH === 'true';
const DEV_SERVER_PORT = 3000;
const DEV_SERVER_HOST = 'localhost';

const postCSSOpts = {
    sourceMap: !isProd,
    ident: 'postcss',
    plugins: [
        postcssImport(),
        autoprefixer(),
        postcssNested()
    ]
};

const projectName = require('./package').name;

const toPascalCase = str => str
    .match(/[a-z]+/gi)
    .map(word => {
        return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
    })
    .join('');


module.exports = (function() {
    const options = {
        watch: isWatch,
        devtool: isProd
            ? false
            : 'inline-source-map',
        stats: {
            children: false
        },
        mode: isProd ? 'none' : 'development',
        entry: {
            [`${projectName}`]: ['./src/index']
        },
        output: {
            path: path.resolve(__dirname, 'public/output/'),
            publicPath: '/',
            library: toPascalCase(projectName),
            libraryTarget: 'umd',
            libraryExport: 'default',
            filename: isProd ? '[name].min.js' : '[name].js'
        },
        resolve: {
            extensions: ['.tsx', '.ts', '.js', '.css', '.json'],
            modules: ['node_modules']
        },
        optimization: {
            minimize: isProd,
            minimizer: [new TerserPlugin(), new OptimizeCSSPlugin()]
        },
        plugins: [
            new webpack.DefinePlugin({
                'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
            }),
            new webpack.NoEmitOnErrorsPlugin(),
            new MiniCssExtractPlugin({
                filename: isProd
                    ? '[name].min.css'
                    : '[name].css',
                chunkFilename: '[id].css'
            })
        ],
        module: {
            rules: [{
                test: /\.tsx?$/,
                use: [{
                    loader: 'awesome-typescript-loader',
                    options: {
                        useCache: true,
                        cacheDirectory: '.awcache'
                    }
                }],
                exclude: /(node_modules)/
            }, {
                test: /\.css$/,
                use: getCSSLoaderRules({
                    cssModulesEnabled: true,
                    shouldExtract: isProd,
                    sourceMap: !isProd
                }),
                exclude: [/(node_modules)/]
            }, {
                test: /\.css$/,
                use: getCSSLoaderRules({
                    cssModulesEnabled: false,
                    shouldExtract: isProd,
                    sourceMap: !isProd
                }),
                include: /(node_modules)/
            }]
        }
    };

    if (isWatch) {
        options.entry[projectName].unshift(
            `webpack-dev-server/client?http://${DEV_SERVER_HOST}:${DEV_SERVER_PORT}`,
            'webpack/hot/only-dev-server'
        );
        options.plugins.push(new webpack.HotModuleReplacementPlugin());
        options.devServer = {
            contentBase: './',
            port: DEV_SERVER_PORT,
            host: DEV_SERVER_HOST,
            hot: true,
            open: false,
            openPage: 'public/index.html',
            stats: { children: false }
        };
    }

    if (isProd) {
        options.plugins.push(
            new webpack.optimize.ModuleConcatenationPlugin()
        );
    } else {
        options.plugins.push(new webpack.NamedModulesPlugin());
    }

    return options;
}());


function getCSSLoaderRules(params) {
    const cssLoader = {
        loader: 'css-loader',
        options: {
            modules: {
                mode: params.cssModulesEnabled ? 'local' : 'global'
            },
            sourceMap: params.sourceMap
        }
    };

    const styleLoader = {
        loader: 'style-loader',
        options: {
            injectType: 'styleTag'
        }
    };

    if (params.cssModulesEnabled) {
        cssLoader.options.modules.localIdentName = '[name]__[local]';
    }

    return [
        params.shouldExtract
            ? { loader: MiniCssExtractPlugin.loader }
            : styleLoader,
        cssLoader,
        {
            loader: 'postcss-loader',
            options: postCSSOpts
        }
    ];

}
